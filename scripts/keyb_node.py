#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from serial import *
import signal
import sys


def talker():
    pub = rospy.Publisher('keyboard/key', String, queue_size=10)
    rospy.init_node('keyb_node', anonymous=True)
    rate = rospy.Rate(20)  # 20hz
    
    while not rospy.is_shutdown():
        ligne = raw_input("key ")
        pub.publish(ligne)
        rate.sleep()


if __name__ == '__main__':
    
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
